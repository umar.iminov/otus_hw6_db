﻿using System;
using System.Linq;
using System.Threading.Tasks;
using OTUS_HW6_DB.Entities;
using Microsoft.EntityFrameworkCore;


namespace OTUS_HW6_DB.Data
{
   public  class DefaultContentMaker
    {
        public async Task MakeAsync(DataContext context)
        {
            try
            {
                if (!context.Sellers.Any())
                {
                    await context.Sellers.AddRangeAsync(GetDefaultSellers());
                    await context.SaveChangesAsync();
                }

                if (!context.Items.Any())
                {
                    await context.Items.AddRangeAsync(GetDefaultItems());
                    await context.SaveChangesAsync();
                }

                if (!context.Offers.Any())
                {
                    await context.Offers.AddRangeAsync(GetDefaultOffers(context.Sellers, context.Items));
                    await context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Ошибка при добавлении начальных данных. Контекст: {nameof(DataContext)} сообщение: {ex.Message}");
            }
        }

        private Offer[] GetDefaultOffers(DbSet<Seller> sellers, DbSet<Item> items)
        {
            var _sellers = sellers.ToList();
            var _items = items.ToList();
            if (_sellers.Count < 5 || _items.Count < 5)
                throw new ArgumentException("Количество продавцов и товаров должно быть не меньше 5");
            return new[]
            {
                new Offer{Seller = _sellers[0], Item = _items[0], Price= 1500},
                new Offer{Seller = _sellers[1], Item = _items[1], Price= 500},
                new Offer{Seller = _sellers[1], Item = _items[2], Price= 400},
                new Offer{Seller = _sellers[2], Item = _items[3], Price= 1000},
                new Offer{Seller = _sellers[2], Item = _items[4], Price= 1700},
                new Offer{Seller = _sellers[3], Item = _items[1], Price= 550},
                new Offer{Seller = _sellers[4], Item = _items[2], Price= 399},
            };
        }

        private Item[] GetDefaultItems()
        {
            return new[]
            {
                new Item {Name = "IPhone 12", Description = "512 Gb"},
                new Item {Name = "Apple Watch", Description = "Series 6"},
                new Item {Name = "Airpods", Description = "wireless charge"},
                new Item {Name = "Ipad", Description = "Apple IPad 12.9"},
                new Item {Name = "Macbook Air", Description = "Macbook Air 13.3" }
            };
        }

        private Seller[] GetDefaultSellers()
        {
            return new[]
            {
                new Seller {Name = "Seller1", Email = "seller1@gmail.com", Phone = "111-11-11"},
                new Seller {Name = "Seller2", Email = "seller2@gmail.com", Phone = "111-11-12"},
                new Seller {Name = "Seller3", Email = "seller3@gmail.com", Phone = "111-11-13"},
                new Seller {Name = "Seller4", Email = "seller4@gmail.com", Phone = "111-11-14"},
                new Seller {Name = "Seller5", Email = "seller5@gmail.com", Phone = "111-11-15"}
            };
        }
    }

}

