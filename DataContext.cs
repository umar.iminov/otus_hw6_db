﻿using Microsoft.EntityFrameworkCore;
using OTUS_HW6_DB.Entities;
using OTUS_HW6_DB.EntityConfigurations;
using System.Configuration;

namespace OTUS_HW6_DB
{
    public class DataContext : DbContext
    {
        public DbSet<Seller> Sellers { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Offer> Offers { get; set; }
                
        public DataContext()
        {
            //Database.EnsureCreated();
            Database.Migrate();
                   
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            string connectionString = ConfigurationManager.AppSettings.Get("ConnectionString");

                //"Host = localhost; Port = 5432; Database = EbayDB; Username = postgres; Password = Start123";

            optionsBuilder.UseNpgsql(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new SellerConfiguration());
            builder.ApplyConfiguration(new ItemConfiguration());
            builder.ApplyConfiguration(new OfferConfiguration());
        }

           
    }
}
