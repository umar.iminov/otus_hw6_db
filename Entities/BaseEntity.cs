﻿using System;

namespace OTUS_HW6_DB.Entities
{
    public class BaseEntity : IEntity
    {
        public long Id { get; set; }
        public DateTime DateCreated { get; set; } = DateTime.Now;
     }
}
