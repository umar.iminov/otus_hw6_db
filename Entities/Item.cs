﻿using System.Collections.Generic;

namespace OTUS_HW6_DB.Entities
{
    public class Item : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<Offer> Sellers { get; set; }
    }
}
