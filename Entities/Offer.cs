﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS_HW6_DB.Entities
{
    public class Offer : BaseEntity
    {
        public Item Item { get; set; }
        public long ItemId { get; set; }
        public decimal Price { get; set; }
        public Seller Seller { get; set; }
        public long SellerId { get; set; }
    }
}
