﻿using System.Collections.Generic;
namespace OTUS_HW6_DB.Entities
{
   public  class Seller : BaseEntity
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public ICollection<Offer> Items { get; set; }
     }
}
