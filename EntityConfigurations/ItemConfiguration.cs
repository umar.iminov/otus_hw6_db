﻿using OTUS_HW6_DB.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace OTUS_HW6_DB.EntityConfigurations
{
   public  class ItemConfiguration : IEntityTypeConfiguration<Item>
    {
        public void Configure (EntityTypeBuilder<Item> builder)
        {
            builder.ToTable("Items")
                .HasComment("Товары");
            builder.HasKey(item => item.Id);
            builder.Property(item => item.DateCreated)
                .HasComment("Дата создания");

            builder.HasData(
                new[]
            {
                new Item {Id= 1, Name = "IPhone 12", Description = "512 Gb"},
                new Item {Id= 2,Name = "Apple Watch", Description = "Series 6"},
                new Item {Id= 3,Name = "Airpods", Description = "wireless charge"},
                new Item {Id= 4,Name = "Ipad", Description = "Apple IPad 12.9"},
                new Item {Id= 5,Name = "Macbook Air", Description = "Macbook Air 13.3" }
            }
                );
        }

    }
}
