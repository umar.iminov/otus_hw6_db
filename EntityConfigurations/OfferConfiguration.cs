﻿using OTUS_HW6_DB.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace OTUS_HW6_DB.EntityConfigurations
{
    class OfferConfiguration : IEntityTypeConfiguration<Offer>
    {
        public void Configure(EntityTypeBuilder<Offer> builder)
        {
            builder.ToTable("Offers")
                .HasComment("Предложения от продавцов");

            builder.HasKey(offer => offer.Id);

            builder.Property(offer => offer.Id)
                .HasColumnName("offer_id")
                .HasComment("PK");

            builder.Property(offer => offer.ItemId)
                .HasColumnName("item_id")
                .HasComment("items FK");
            builder.HasOne(a => a.Item)
                .WithMany(o => o.Sellers)
                .HasForeignKey(k => k.ItemId);

            builder.Property(offer => offer.SellerId)
                .HasColumnName("seller_id")
                .HasComment("sellers FK");
            builder.HasOne(a => a.Seller)
                .WithMany(i => i.Items)
                .HasForeignKey(offer => offer.SellerId);
                       
        }


    }
}
