﻿using OTUS_HW6_DB.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace OTUS_HW6_DB.EntityConfigurations
{
    class SellerConfiguration : IEntityTypeConfiguration<Seller>
    {
        public void Configure(EntityTypeBuilder<Seller> builder)
        {
            builder.ToTable("Sellers")
                .HasComment("Продавцы");

            builder.HasKey(seller => seller.Id);

            builder.Property(seller => seller.Id)
                .HasColumnName("seller_id")
                .HasComment("PK");

            builder.Property(seller => seller.Name)
                .HasColumnName("firstname")
                .HasComment("First name");

            builder.Property(student => student.DateCreated)
                .HasColumnName("date_created")
                .HasComment("Дата регистрации");

            builder.HasData(
            new[]
            {
                new Item {Id= 1,Name = "IPhone 12", Description = "512 Gb"},
                new Item {Id= 2,Name = "Apple Watch", Description = "Series 6"},
                new Item {Id= 3,Name = "Airpods", Description = "wireless charge"},
                new Item {Id= 4,Name = "Ipad", Description = "Apple IPad 12.9"},
                new Item {Id= 5,Name = "Macbook Air", Description = "Macbook Air 13.3" }
            }
                );

        }
    } 

}

