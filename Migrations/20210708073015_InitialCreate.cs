﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace OTUS_HW6_DB.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Items",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: true),
                    Description = table.Column<string>(type: "text", nullable: true),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false, comment: "Дата создания")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Items", x => x.Id);
                },
                comment: "Товары");

            migrationBuilder.CreateTable(
                name: "Sellers",
                columns: table => new
                {
                    seller_id = table.Column<long>(type: "bigint", nullable: false, comment: "PK")
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    firstname = table.Column<string>(type: "text", nullable: true, comment: "First name"),
                    Email = table.Column<string>(type: "text", nullable: true),
                    Phone = table.Column<string>(type: "text", nullable: true),
                    date_created = table.Column<DateTime>(type: "timestamp without time zone", nullable: false, comment: "Дата регистрации")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sellers", x => x.seller_id);
                },
                comment: "Продавцы");

            migrationBuilder.CreateTable(
                name: "Offers",
                columns: table => new
                {
                    offer_id = table.Column<long>(type: "bigint", nullable: false, comment: "PK")
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    item_id = table.Column<long>(type: "bigint", nullable: false, comment: "items FK"),
                    Price = table.Column<decimal>(type: "numeric", nullable: false),
                    seller_id = table.Column<long>(type: "bigint", nullable: false, comment: "sellers FK"),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Offers", x => x.offer_id);
                    table.ForeignKey(
                        name: "FK_Offers_Items_item_id",
                        column: x => x.item_id,
                        principalTable: "Items",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Offers_Sellers_seller_id",
                        column: x => x.seller_id,
                        principalTable: "Sellers",
                        principalColumn: "seller_id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "Предложения от продавцов");

            migrationBuilder.InsertData(
                table: "Items",
                columns: new[] { "Id", "DateCreated", "Description", "Name" },
                values: new object[,]
                {
                    { 1L, new DateTime(2021, 7, 8, 13, 30, 14, 728, DateTimeKind.Local).AddTicks(1230), "512 Gb", "IPhone 12" },
                    { 2L, new DateTime(2021, 7, 8, 13, 30, 14, 728, DateTimeKind.Local).AddTicks(1237), "Series 6", "Apple Watch" },
                    { 3L, new DateTime(2021, 7, 8, 13, 30, 14, 728, DateTimeKind.Local).AddTicks(1238), "wireless charge", "Airpods" },
                    { 4L, new DateTime(2021, 7, 8, 13, 30, 14, 728, DateTimeKind.Local).AddTicks(1239), "Apple IPad 12.9", "Ipad" },
                    { 5L, new DateTime(2021, 7, 8, 13, 30, 14, 728, DateTimeKind.Local).AddTicks(1240), "Macbook Air 13.3", "Macbook Air" }
                });

            migrationBuilder.InsertData(
                table: "Sellers",
                columns: new[] { "seller_id", "date_created", "Email", "firstname", "Phone" },
                values: new object[,]
                {
                    { 1L, new DateTime(2021, 7, 8, 13, 30, 14, 726, DateTimeKind.Local).AddTicks(58), null, "IPhone 12", null },
                    { 2L, new DateTime(2021, 7, 8, 13, 30, 14, 726, DateTimeKind.Local).AddTicks(8074), null, "Apple Watch", null },
                    { 3L, new DateTime(2021, 7, 8, 13, 30, 14, 726, DateTimeKind.Local).AddTicks(8089), null, "Airpods", null },
                    { 4L, new DateTime(2021, 7, 8, 13, 30, 14, 726, DateTimeKind.Local).AddTicks(8091), null, "Ipad", null },
                    { 5L, new DateTime(2021, 7, 8, 13, 30, 14, 726, DateTimeKind.Local).AddTicks(8092), null, "Macbook Air", null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Offers_item_id",
                table: "Offers",
                column: "item_id");

            migrationBuilder.CreateIndex(
                name: "IX_Offers_seller_id",
                table: "Offers",
                column: "seller_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Offers");

            migrationBuilder.DropTable(
                name: "Items");

            migrationBuilder.DropTable(
                name: "Sellers");
        }
    }
}
