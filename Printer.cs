﻿using System;
using OTUS_HW6_DB.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;


namespace OTUS_HW6_DB
{
   public static  class Printer
    {
        public static void PrintAllOffers(IQueryable<Offer> offers)
        {
            Console.WriteLine();
            Console.WriteLine("Текущие предложения магазина:");
            foreach (var offer in offers)
            {
                Console.WriteLine($"Id: {offer.Id}, товар: {offer.Item.Name}, " +
                                  $"Описание товара: {offer.Item.Description}, " +
                                  $"Цена: {offer.Price}, " +
                                  $"Продавец: {offer.Seller.Name}, дата размещения: {offer.DateCreated}");
            }

        }

        public static void PrintWelcome()
        {
            Console.WriteLine("Введите команду, чтобы выбрать таблицу для добавления данных (пустая строка - выход):");
            Console.WriteLine("   item - Товары");
            Console.WriteLine("   seller - Продавцы");
            Console.WriteLine("   offer - Предложения от продавцов");
        }
        
        public static void PrintAllItems(DbSet<Item> items)
        {
            Console.WriteLine();
            Console.WriteLine("Таблица Items содержит следующие данные:");
            foreach (var item in items)
            {
                Console.WriteLine($"   Id: {item.Id}, название: {item.Name}, описание: {item.Description}, дата добавления {item.DateCreated}");
            }
        }

        public static void PrintAllSellers(DbSet<Seller> sellers)
        {
            Console.WriteLine();
            Console.WriteLine("Таблица Sellers содержит следующие данные:");
            foreach (var seller in sellers)
            {
                Console.WriteLine($"   Id: {seller.Id}, имя: {seller.Name}, тел: {seller.Phone}, email: {seller.Email}, дата регистрации: {seller.DateCreated} ");
            }
        }



    }
}
