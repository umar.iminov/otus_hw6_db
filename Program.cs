﻿using System;
using OTUS_HW6_DB.Entities;
using Microsoft.EntityFrameworkCore;
using OTUS_HW6_DB.Data;
namespace OTUS_HW6_DB
{
    class Program
    {
        static void Main()
        {
            using (var context = new DataContext())
            {
                new DefaultContentMaker().MakeAsync(context).ConfigureAwait(false).GetAwaiter().GetResult();
                Printer.PrintAllSellers(context.Sellers);
                Printer.PrintAllItems(context.Items);
                Printer.PrintAllOffers(context.Offers);

                do
                {
                    Printer.PrintWelcome();
                    var s = Console.ReadLine();
                    if (string.IsNullOrEmpty(s))
                        return;
                    
                    switch (s)
                    {
                        case "seller":
                            if (!AddSeller(context.Sellers))
                                return;
                            Console.WriteLine("Запись вставлена");
                            context.SaveChanges();
                            Printer.PrintAllSellers(context.Sellers);
                            break;

                        case "item":
                            if (!AddItem(context.Items))
                                return;
                            Console.WriteLine("Запись вставлена");
                            context.SaveChanges();
                            Printer.PrintAllItems(context.Items);
                            break;

                        case "offer":
                            if (!AddOffer(context.Offers))
                                return;
                            Console.WriteLine("Запись вставлена");
                            context.SaveChanges();
                            Printer.PrintAllOffers(context.Offers);
                            break;
                        default:
                            Console.WriteLine("Неверная команда");
                            break;
                    }
                } while (true);
            }
        }

        private static bool AddOffer(DbSet<Offer> offers)
        {
            
            do
            {
                Console.WriteLine("Введите <Id продавца> ; <Id товара> ; <Цена>  (пустая строка - выход)");
                var s = Console.ReadLine();
                if (string.IsNullOrEmpty(s))
                    return false;

                var data = s.Split(";");
                if (data.Length != 3 || !int.TryParse(data[0], out var sellerId) ||
                    !int.TryParse(data[1], out var itemId)|| !int.TryParse(data[2], out var price))
                {
                    Console.WriteLine("Необходимо ввести три числа");
                    continue;
                }

                try
                {
                    var offer = new Offer { SellerId = sellerId, ItemId = itemId, Price= price};
                    offers.Add(offer);
                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Произошла ошибка при добавлении записи в БД. {e.Message} Попробуйте ещё раз.");
                }
            } while (true);
        }

        private static bool AddItem(DbSet<Item> items)
        {
            Console.WriteLine("Введите <название> ; <описание> (пустая строка - выход)");
            do
            {
                var s = Console.ReadLine();
                if (string.IsNullOrEmpty(s))
                    return false;
                var data = s.Split(";");
                if (data.Length != 2)
                {
                    Console.WriteLine("Необходимо ввести две строки разделенные ; ");
                    continue;
                }
                var name = data[0];
                var dscr = data[1];

                try
                {
                    var item = new Item { Name = name, Description = dscr};
                    items.Add(item);
                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Произошла ошибка при добавлении записи в БД. {e.Message} Попробуйте ещё раз.");
                }
            } while (true);
        }

        private static bool AddSeller(DbSet<Seller> sellers)
        {
            Console.WriteLine("Введите  <Имя>; <Email>; <Телефон> (пустая строка - выход)");
            do
            {
                var s = Console.ReadLine();
                if (string.IsNullOrEmpty(s))
                    return false;

                var data = s.Split(";");
                if (data.Length != 3)
                {
                    Console.WriteLine("Необходимо ввести три строки, разделенных ;");
                    continue;
                }

                var name = data[0];
                var email = data[1];
                var phone = data[2];

                try
                {
                    var seller = new Seller { Name = name, Email = email, Phone= phone };
                    sellers.Add(seller);
                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Произошла ошибка при добавлении записи в БД. {e.Message} Попробуйте ещё раз.");
                }
            } while (true);

        }

    }
}